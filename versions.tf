terraform {
  backend "s3" {
    bucket = "own-terraform-state-bucket"
    key    = "eks-cluster/terraform.tfstate"
    region = "us-east-2"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.39.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.10.0"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.7.1"
    }
  }
}
