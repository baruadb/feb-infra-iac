# resource "kubernetes_service" "frontend_svc" {
#   metadata {
#     name = "frontend-svc"
#   }
#   spec {
#     selector = {
#       test = kubernetes_deployment.frontend.metadata.0.labels.test
#     }
#     port {
#       port        = 80
#     }

#     type = "NodePort"
#   }
# }

resource "kubernetes_service" "backend_svc" {
  metadata {
    name = "backend-svc"
  }
  spec {
    selector = {
      test = kubernetes_deployment.backend.metadata.0.labels.test
    }
    port {
      port        = 5000
    }

    type = "NodePort"
  }
}
