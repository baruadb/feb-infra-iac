resource "kubernetes_deployment" "backend" {
  metadata {
    name = "backend"
    labels = {
      test = "backend"
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        test = "backend"
      }
    }

    template {
      metadata {
        labels = {
          test = "backend"
        }
      }

      spec {
        container {
          image = "debashishbarua123/tf-two-tier-backend:${var.backend_tag}"
          name  = "backend"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

##     }
#   }

#   spec {
#     replicas = 2

#     selector {
#       match_labels = {
#         test = "frontend"
#       }
#     }

#     template {
#       metadata {
#         labels = {
#           test = "frontend"
#         }
#       }

#       spec {
#         container {
#           image = "shahedmehbub/tf-two-tier-frontend"
 resource "kubernetes_deployment" "frontend" {
#   metadata {
#     name = "frontend"
#     labels = {
#       test = "frontend"
#           name  = "frontend"

#           resources {
#             limits = {
#               cpu    = "0.5"
#               memory = "512Mi"
#             }
#             requests = {
#               cpu    = "250m"
#               memory = "50Mi"
#             }
#           }
#         }
#       }
#     }
#   }
# }
